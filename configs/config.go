package configs

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/jakinss321/api-getway/models"
)

func init() {
	if err := godotenv.Load(); err != nil {
		log.Fatal("Error loading .env file")
	}
}

func InitConfig() *models.Cfg {
	return &models.Cfg{
		UserService: os.Getenv("user"),
		UserPort:    os.Getenv("portGetway"),
		AuthService: os.Getenv("auth"),
		AuthPort:    os.Getenv("portauth"),
		PortGetway:  os.Getenv("portGetway"),
		Ifgrpc:      os.Getenv("ifgrpc"),
		SSLMode:     os.Getenv("SSLMODEDB"),
		PasswordDB:  os.Getenv("PASSWORDDB"),
		PortDB:      os.Getenv("PORTDB"),
		Token:       os.Getenv("TOKEN"),
	}
	// portGetway=8080
	// auth=authapi
	// portauth=3456
	// user=userapi
	// portuser=3455
	// ifgrpc = grpc

}

// cfg := models.Cfg{
// 	UserService: configs.User,
// 	UserPort:    configs.PortUser,
// 	AuthService: configs.Auth,
// 	AuthPort:    configs.PortAuth,
// 	IfGRPC:      configs.Ifgrpc,
// }
