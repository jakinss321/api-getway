package models

type Cfg struct {
	PortGetway  string
	PortUser    string
	Ifgrpc      string
	HostDB      string
	PortDB      string
	UserDB      string
	SSLMode     string
	PasswordDB  string
	UserService string
	UserPort    string
	AuthService string
	AuthPort    string
	Token       string
	IfGRPC      string
}

type RegisterIn struct {
	Email          string
	Phone          string
	Password       string
	IdempotencyKey string
}

type RegisterOut struct {
	Status    int
	ErrorCode int
}

type AuthorizeEmailIn struct {
	Email          string
	Password       string
	RetypePassword string
}

type AuthorizeOut struct {
	UserID       int
	AccessToken  string
	RefreshToken string
	ErrorCode    int
}

type UserTg struct {
	ChatID int64
}
