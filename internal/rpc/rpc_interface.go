package rpc

import (
	"net/rpc"

	"github.com/gin-gonic/gin"
	"gitlab.com/jakinss321/api-getway/models"
	servicepbauth "gitlab.com/jakinss321/microservice_auth/grpc/auth"
	servicepbexchange "gitlab.com/jakinss321/microservice_exchange/grpc/exchange"
	modelsExchange "gitlab.com/jakinss321/microservice_exchange/models"
	servicepb "gitlab.com/jakinss321/microservice_user/grpc/user"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/tools/cryptography"
	"gitlab.com/jakinss321/microservice_user/pkg/modules/user/service"
)

type RPCClient interface {
	CheckToken(token string) (*cryptography.UserClaims, error)
	UserProfile(c *gin.Context, usr *cryptography.UserClaims) (*service.UserOut, error)
	Register(c *gin.Context, usr *models.RegisterIn) (models.RegisterOut, error)
	Login(c *gin.Context, in models.AuthorizeEmailIn) (models.AuthorizeOut, error)

	GetTicker() (*modelsExchange.TickerResponse, error)
	GetHistory() *modelsExchange.PriceHistories
	GetMaxPrice() *modelsExchange.MaxPrices
	GetMinPrice() *modelsExchange.MinPrices
	GetAveragePrice() *modelsExchange.AveragePrices
}

type Rpc struct {
	cfg               *models.Cfg
	userClientRpc     *rpc.Client
	authClientRpc     *rpc.Client
	exchangeClientRPC *rpc.Client
}

type Grpc struct {
	cfg                       *models.Cfg
	userServiceClientGRPC     servicepb.UserServiceClient
	authServiceClientGRPC     servicepbauth.AuthServiceClient
	exchangeServiceClientGRPC servicepbexchange.ExchangeServiceClient
}
