package rpc

import (
	"context"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/jakinss321/api-getway/models"
	servicepbauth "gitlab.com/jakinss321/microservice_auth/grpc/auth"
	servicepbexchange "gitlab.com/jakinss321/microservice_exchange/grpc/exchange"
	modelsExchange "gitlab.com/jakinss321/microservice_exchange/models"
	servicepb "gitlab.com/jakinss321/microservice_user/grpc/user"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/tools/cryptography"
	m "gitlab.com/jakinss321/microservice_user/pkg/models"
	"gitlab.com/jakinss321/microservice_user/pkg/modules/user/service"
	"google.golang.org/grpc"
)

func NewGRPC(cfg *models.Cfg) (*Grpc, error) {
	userClient, err := grpc.Dial("userapi:3455", grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	logrus.Printf("Connected to JSON-RPC service at userapi:3455")
	userServiceClient := servicepb.NewUserServiceClient(userClient)

	authClient, err := grpc.Dial("authapi:3456", grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	logrus.Printf("Connected to GSON-RPC service at authapi:3456")
	authServiceClient := servicepbauth.NewAuthServiceClient(authClient)

	exchangeClient, err := grpc.Dial("exchangeapi:3457", grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	logrus.Printf("Connected to GSON-RPC service at exchangeapi:3457")
	exchangeServiceClient := servicepbexchange.NewExchangeServiceClient(exchangeClient)

	return &Grpc{
		cfg:                       cfg,
		userServiceClientGRPC:     userServiceClient,
		authServiceClientGRPC:     authServiceClient,
		exchangeServiceClientGRPC: exchangeServiceClient,
	}, nil

}

func (h *Grpc) CheckToken(token string) (*cryptography.UserClaims, error) {

	in := &servicepb.TokenRequest{
		Token: token,
	}

	res, err := h.userServiceClientGRPC.ParseToken(context.Background(), in)
	if err != nil {
		return nil, err
	}
	out := &cryptography.UserClaims{
		ID:     res.Id,
		Role:   res.Role,
		Groups: res.Groups,
		Layers: res.Layers,
	}

	return out, nil
}

func (h *Grpc) UserProfile(c *gin.Context, usr *cryptography.UserClaims) (*service.UserOut, error) {
	usrid, err := strconv.Atoi(usr.ID)
	if err != nil {
		return nil, err
	}
	in := &servicepb.GetByIDRequest{
		UserId: int32(usrid),
	}
	res, err := h.userServiceClientGRPC.GetUserByID(c, in)
	if err != nil {
		return nil, err
	}

	out := &service.UserOut{
		User: &m.User{
			ID:            int(res.User.Id),
			Name:          res.User.Name,
			Phone:         res.User.Phone,
			Email:         res.User.Email,
			Password:      res.User.Password,
			Role:          int(res.User.Role),
			Verified:      res.User.Verified,
			EmailVerified: res.User.EmailVerified,
			PhoneVerified: res.User.PhoneVerified,
		}, ErrorCode: int(res.ErrorCode),
	}
	return out, nil
}
func (h *Grpc) Register(c *gin.Context, usr *models.RegisterIn) (models.RegisterOut, error) {
	in := &servicepbauth.RegisterRequest{
		Email:    usr.Email,
		Phone:    usr.Phone,
		Password: usr.Password,
	}
	_, err := h.authServiceClientGRPC.Register(c, in)
	if err != nil {
		return models.RegisterOut{
			ErrorCode: 403,
		}, err
	}
	return models.RegisterOut{
		Status: 200,
	}, nil
}
func (h *Grpc) Login(c *gin.Context, usr models.AuthorizeEmailIn) (models.AuthorizeOut, error) {
	in := &servicepbauth.LoginRequest{
		Email:    usr.Email,
		Password: usr.Password,
	}

	out, err := h.authServiceClientGRPC.Login(c, in)
	if err != nil {
		return models.AuthorizeOut{}, err
	}
	return models.AuthorizeOut{
		UserID:       int(out.Id),
		AccessToken:  out.Accesstoken,
		RefreshToken: out.Refreshtoken,
		ErrorCode:    int(out.Errorcode),
	}, nil
}

func (h *Grpc) GetTicker() (*modelsExchange.TickerResponse, error) {
	in := &servicepbexchange.FetchTickerDataRequest{}
	res, err := h.exchangeServiceClientGRPC.FetchTickerData(context.Background(), in)
	if err != nil {
		return nil, err
	}

	tickerResponse := make(modelsExchange.TickerResponse)

	for symbol, tickerData := range res.Data {
		tickerResponse[symbol] = modelsExchange.TickerData{
			BuyPrice:  float64(tickerData.BuyPrice),
			SellPrice: float64(tickerData.SellPrice),
			LastTrade: float64(tickerData.LastTrade),
			High:      float64(tickerData.High),
			Low:       float64(tickerData.Low),
			Avg:       float64(tickerData.Avg),
			Vol:       float64(tickerData.Vol),
			VolCurr:   float64(tickerData.VolCurr),
			Updated:   tickerData.Updated,
		}
	}
	return &tickerResponse, nil
}
func (h *Grpc) GetHistory() *modelsExchange.PriceHistories {
	in := &servicepbexchange.GetHistoryRequest{}
	res, err := h.exchangeServiceClientGRPC.GetHistory(context.Background(), in)
	if err != nil {
		return nil
	}

	priceHistories := make(modelsExchange.PriceHistories, len(res.Histories))

	for i, ph := range res.Histories {
		priceHistory := struct {
			Pair     string    `json:"pair"`
			High     float64   `json:"high"`
			Low      float64   `json:"low"`
			DateTime time.Time `json:"date_time"`
		}{
			Pair:     ph.Pair,
			High:     float64(ph.High),
			Low:      float64(ph.Low),
			DateTime: ph.DateTime.AsTime(),
		}

		priceHistories[i] = priceHistory
	}

	return &priceHistories
}

func (h *Grpc) GetMaxPrice() *modelsExchange.MaxPrices {
	in := &servicepbexchange.GetMaxPriceRequest{}

	res, err := h.exchangeServiceClientGRPC.GetMaxPrice(context.Background(), in)
	if err != nil {
		return nil
	}

	maxPrices := make(modelsExchange.MaxPrices, len(res.Prices))
	for i, mp := range res.Prices {
		MaxPrice := struct {
			Pair  string  `json:"pair"`
			Price float64 `json:"price"`
		}{
			Pair:  mp.Pair,
			Price: float64(mp.Price),
		}
		maxPrices[i] = MaxPrice
	}

	return &maxPrices
}

func (h *Grpc) GetMinPrice() *modelsExchange.MinPrices {
	in := &servicepbexchange.GetMinPriceRequest{}

	res, err := h.exchangeServiceClientGRPC.GetMinPrice(context.Background(), in)
	if err != nil {
		return nil
	}

	minPrices := make(modelsExchange.MinPrices, len(res.Prices))
	for i, mp := range res.Prices {
		MinPrice := struct {
			Pair  string  `json:"pair"`
			Price float64 `json:"price"`
		}{
			Pair:  mp.Pair,
			Price: float64(mp.Price),
		}
		minPrices[i] = MinPrice
	}

	return &minPrices
}
func (h *Grpc) GetAveragePrice() *modelsExchange.AveragePrices {
	in := &servicepbexchange.GetAveragePriceRequest{}

	res, err := h.exchangeServiceClientGRPC.GetAveragePrice(context.Background(), in)
	if err != nil {
		return nil
	}

	avPrices := make(modelsExchange.AveragePrices, len(res.Prices))
	for i, mp := range res.Prices {
		avPrice := struct {
			Pair         string  `json:"pair"`
			AveragePrice float64 `json:"average_price"`
		}{
			Pair:         mp.Pair,
			AveragePrice: float64(mp.AveragePrice),
		}
		avPrices[i] = avPrice
	}

	return &avPrices
}
