package rpc

import (
	"errors"
	"fmt"
	"net/rpc/jsonrpc"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/jakinss321/api-getway/models"
	modelsExchange "gitlab.com/jakinss321/microservice_exchange/models"

	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/tools/cryptography"
	"gitlab.com/jakinss321/microservice_user/pkg/modules/user/service"
)

func NewRpc(cfg *models.Cfg) (*Rpc, error) {
	userClient, err := jsonrpc.Dial("tcp", "userapi:3455")
	if err != nil {
		return nil, err
	}
	logrus.Printf("Connected to JSON-RPC service at userapi:3455")

	authClient, err := jsonrpc.Dial("tcp", "authapi:3456")
	if err != nil {
		return nil, err
	}
	logrus.Printf("Connected to JSON-RPC service at authapi:3456")

	exchangeClient, err := jsonrpc.Dial("tcp", "exchangeapi:3457")
	if err != nil {
		return nil, err
	}
	logrus.Printf("Connected to JSON-RPC service at exchangeapi:3457")

	return &Rpc{
		cfg:               cfg,
		userClientRpc:     userClient,
		authClientRpc:     authClient,
		exchangeClientRPC: exchangeClient,
	}, nil
}

func (h *Rpc) CheckToken(in string) (*cryptography.UserClaims, error) {

	out := &cryptography.UserClaims{}

	err := h.userClientRpc.Call("UserServiceJSONRPC.ParseToken", in, &out)
	if err != nil {
		return out, err
	}

	if out.ID == "" {
		return out, errors.New("token exp")
	}
	return out, nil
}

func (h *Rpc) UserProfile(c *gin.Context, usr *cryptography.UserClaims) (*service.UserOut, error) {
	var out service.UserOut
	idInt, _ := strconv.Atoi(usr.ID)
	in := &service.GetByIDIn{
		UserID: idInt,
	}
	err := h.userClientRpc.Call("UserServiceJSONRPC.GetUserByID", in, &out)
	if err != nil {
		return &out, err
	}

	return &out, nil
}

func (h *Rpc) Register(c *gin.Context, in *models.RegisterIn) (models.RegisterOut, error) {
	out := &models.RegisterOut{}
	err := h.authClientRpc.Call("AuthServiceJSONRPC.Register", in, &out)
	if err != nil {
		fmt.Println(err, "err")
		return *out, err
	}

	return *out, nil
}

func (h *Rpc) Login(c *gin.Context, in models.AuthorizeEmailIn) (models.AuthorizeOut, error) {
	out := &models.AuthorizeOut{}
	err := h.authClientRpc.Call("AuthServiceJSONRPC.Login", in, &out)
	if err != nil {
		fmt.Println(err, "err")
		return *out, err
	}
	return *out, nil
}

func (h *Rpc) GetHistory() *modelsExchange.PriceHistories {
	out := &modelsExchange.PriceHistories{}
	err := h.authClientRpc.Call("ExchangeServiceJSONRPC.GetHistory", out, &out)
	if err != nil {
		fmt.Println(err, "err")
		return out
	}
	return out
}

func (h *Rpc) GetMaxPrice() *modelsExchange.MaxPrices {
	out := &modelsExchange.MaxPrices{}
	err := h.authClientRpc.Call("ExchangeServiceJSONRPC.GetMaxPrice", out, &out)
	if err != nil {
		fmt.Println(err, "err")
		return out
	}
	return out
}
func (h *Rpc) GetMinPrice() *modelsExchange.MinPrices {
	out := &modelsExchange.MinPrices{}
	err := h.authClientRpc.Call("ExchangeServiceJSONRPC.GetMinPrice", out, &out)
	if err != nil {
		fmt.Println(err, "err")
		return out
	}
	return out
}
func (h *Rpc) GetAveragePrice() *modelsExchange.AveragePrices {
	out := &modelsExchange.AveragePrices{}
	err := h.authClientRpc.Call("ExchangeServiceJSONRPC.GetAveragePrice", out, &out)
	if err != nil {
		fmt.Println(err, "err")
		return out
	}
	return out
}

func (h *Rpc) GetTicker() (*modelsExchange.TickerResponse, error) {
	out := &modelsExchange.TickerResponse{}
	err := h.exchangeClientRPC.Call("ExchangeServiceJSONRPC.FetchTickerData", &out, &out)
	if err != nil {
		fmt.Println(err, "err")
		return out, err
	}
	return out, nil
}
