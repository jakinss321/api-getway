package handler

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/jakinss321/api-getway/docs"
	"gitlab.com/jakinss321/api-getway/internal/middleware/auth"
	"gitlab.com/jakinss321/api-getway/internal/rpc"
)

type Handler struct {
	auth *auth.AuthMiddleware
	conn rpc.RPCClient
}

func NewHandler(auth *auth.AuthMiddleware, conn rpc.RPCClient) *Handler {
	return &Handler{
		auth: auth,
		conn: conn,
	}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.Default()
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	authGroup := router.Group("/auth")
	{
		authGroup.POST("/register", h.Register)
		authGroup.POST("/login", h.Login)
	}
	userGroup := router.Group("/user", h.auth.AuthMiddleware())
	{
		userGroup.GET("/profile", h.ProfileUser)
	}
	exchangeGroup := router.Group("/exchange", h.auth.AuthMiddleware())
	{
		exchangeGroup.GET("/ticker", h.GetTickerHandler)
		exchangeGroup.GET("/history", h.GetHistoryHandler)
		exchangeGroup.GET("/max-price", h.GetMaxPriceHandler)
		exchangeGroup.GET("/min-price", h.GetMinPriceHandler)
		exchangeGroup.GET("/average-price", h.GetAveragePriceHandler)
	}

	return router
}
