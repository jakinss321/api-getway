package handler

import (
	"fmt"
	"net/http"
	"net/mail"

	"github.com/gin-gonic/gin"
	"gitlab.com/jakinss321/api-getway/models"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/errors"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/tools/cryptography"
)

type UserHandler interface {
	Register(c *gin.Context)
	Login(c *gin.Context)
	ProfileUser(c *gin.Context)
}

// @Summary Register user
// @Description Регистрация пользователя
// @Tags auth
// @Accept json
// @Produce json
// @Param body body RegisterRequest true "Register Request"
// @Success 200 {object} RegisterResponse
// @Router /auth/register [post]
func (a *Handler) Register(c *gin.Context) {
	var req RegisterRequest
	err := c.BindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "bad request",
		})
		return
	}

	if !valid(req.Email) {
		c.JSON(http.StatusBadRequest, RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: Data{
				Message: "invalid email",
			},
		})
		return
	}

	if req.Password != req.RetypePassword {
		c.JSON(http.StatusBadRequest, RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: Data{
				Message: "passwords mismatch",
			},
		})
		return
	}

	out, _ := a.conn.Register(c, &models.RegisterIn{
		Email:    req.Email,
		Password: req.Password,
	})

	if out.ErrorCode != errors.NoError {
		msg := "register error"
		if out.ErrorCode == errors.UserServiceUserAlreadyExists {
			msg = "User already exists, please check your email"
		}
		c.JSON(http.StatusBadRequest, RegisterResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: msg,
			},
		})
		return
	}

	c.JSON(http.StatusOK, RegisterResponse{
		Success: true,
		Data: Data{
			Message: "verification link sent to " + req.Email,
		},
	})
}

// @Summary Login user
// @Description Авторизация пользователя
// @Tags auth
// @Accept json
// @Produce json
// @Param body body LoginRequest true "Login Request"
// @Success 200 {object} AuthResponse
// @Router /auth/login [post]
func (a *Handler) Login(c *gin.Context) {
	var req LoginRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "bad request",
		})
		return
	}
	if len(req.Email) < 5 {
		c.JSON(403, RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: Data{
				Message: "phone or email empty",
			},
		})
	}

	out, _ := a.conn.Login(c, models.AuthorizeEmailIn{
		Email:    req.Email,
		Password: req.Password,
	})
	if out.ErrorCode == errors.AuthServiceUserNotVerified {
		c.JSON(403, AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: LoginData{
				Message: "user email is not verified",
			},
		})
		return
	}

	if out.ErrorCode != errors.NoError {
		c.JSON(403, AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: LoginData{
				Message: "login or password mismatch",
			},
		})
		return
	}

	c.JSON(200, AuthResponse{
		Success: true,
		Data: LoginData{
			Message:      "success login",
			AccessToken:  out.AccessToken,
			RefreshToken: out.RefreshToken,
		},
	})
}

type UserOut struct {
	User      *User `json:"user"`
	ErrorCode int   `json:"error_code"`
}

type User struct {
	ID            int    `json:"id"`
	Name          string `json:"name"`
	Phone         string `json:"phone"`
	Email         string `json:"email"`
	Password      string `json:"password"`
	Role          int    `json:"role"`
	Verified      bool   `json:"verified"`
	EmailVerified bool   `json:"email_verified"`
	PhoneVerified bool   `json:"phone_verified"`
}

// @Summary Profile user
// @Security ApiKeyAuth
// @Description Получение профиля пользователя.
// @Tags user
// @Accept json
// @Produce json
// @Success 200 {object} UserOut
// @Router /user/profile [get]
func (h *Handler) ProfileUser(c *gin.Context) {
	user, exists := c.Get("user")
	if !exists {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "User not found in context"})
		return
	}
	userToken, ok := user.(*cryptography.UserClaims)
	if !ok {
		fmt.Println("userToken error")

		return
	}
	user, err := h.conn.UserProfile(c, userToken)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "User not found in context"})
	}
	c.JSON(http.StatusOK, gin.H{"message": "ProfileUser", "user": user})
}

func valid(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}
