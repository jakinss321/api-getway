package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	modelsExchange "gitlab.com/jakinss321/microservice_exchange/models"
)

// @Summary Возвращает данные тикера.
// @Security ApiKeyAuth
// @Description Возвращает данные тикера.
// @Tags Ticker
// @Produce json
// @Success 200 {object} modelsExchange.TickerResponse
// @Router /exchange/ticker [get]
func (h *Handler) GetTickerHandler(c *gin.Context) {
	ticker := &modelsExchange.TickerResponse{}
	ticker, err := h.conn.GetTicker()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Ошибка при получении данных тикера"})
		return
	}
	c.JSON(http.StatusOK, ticker)
}

// GetHistory возвращает историю цен.
// @Summary Возвращает историю цен.
// @Security ApiKeyAuth
// @Description Возвращает историю цен.
// @Tags Price
// @Produce json
// @Success 200 {object} modelsExchange.PriceHistories
// @Router /exchange/history [get]
func (h *Handler) GetHistoryHandler(c *gin.Context) {
	c.JSON(http.StatusOK, h.conn.GetHistory())
}

// GetMaxPrice возвращает максимальную цену.
// @Summary Возвращает максимальную цену.
// @Security ApiKeyAuth
// @Description Возвращает максимальную цену.
// @Tags Price
// @Produce json
// @Success 200 {object} modelsExchange.MaxPrices
// @Router /exchange/max-price [get]
func (h *Handler) GetMaxPriceHandler(c *gin.Context) {
	c.JSON(http.StatusOK, h.conn.GetMaxPrice())
}

// GetMinPrice возвращает минимальную цену.
// @Summary Возвращает минимальную цену.
// @Security ApiKeyAuth
// @Description Возвращает минимальную цену.
// @Tags Price
// @Produce json
// @Success 200 {object} modelsExchange.MinPrices
// @Router /exchange/min-price [get]
func (h *Handler) GetMinPriceHandler(c *gin.Context) {
	c.JSON(http.StatusOK, h.conn.GetMinPrice())
}

// GetAveragePrice возвращает среднюю цену.
// @Summary Возвращает среднюю цену.
// @Security ApiKeyAuth
// @Description Возвращает среднюю цену.
// @Tags Price
// @Produce json
// @Success 200 {object} modelsExchange.AveragePrices
// @Router /exchange/average-price [get]
func (h *Handler) GetAveragePriceHandler(c *gin.Context) {
	c.JSON(http.StatusOK, h.conn.GetAveragePrice())
}
