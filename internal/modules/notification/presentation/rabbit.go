package presentation

import (
	"log"

	"github.com/streadway/amqp"
)

func (a *App) InitRabbit() {
	conn, ch := a.initRabbitMQ()
	defer conn.Close()
	defer ch.Close()

	msgs := a.consumeMessages(ch)
	a.processAndSendMessages(msgs)

	select {}
}

func (a *App) initRabbitMQ() (*amqp.Connection, *amqp.Channel) {
	conn, err := amqp.Dial("amqp://guest:guest@rabbitmq:5672/")
	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %v", err)
	}

	ch, err := conn.Channel()
	if err != nil {
		log.Fatalf("Failed to open a channel: %v", err)
	}

	_, err = ch.QueueDeclare(
		"telegram-messages",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to declare a queue: %v", err)
	}

	return conn, ch
}

func (a *App) consumeMessages(ch *amqp.Channel) <-chan amqp.Delivery {
	msgs, err := ch.Consume(
		"telegram-messages",
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to register a consumer: %v", err)
	}

	return msgs
}

func (a *App) processAndSendMessages(msgs <-chan amqp.Delivery) {
	go func() {
		for msg := range msgs {
			text := string(msg.Body)
			log.Printf("Received a message: %s\n", text)
			err := a.appService.AllUsersMessages(text)
			if err != nil {
				log.Fatalf("Failed to send message: %v", err)
			}

		}
	}()

	log.Printf("Listening for messages on queue '%s'\n", "telegram-messages")
}
