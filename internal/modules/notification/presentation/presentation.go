package presentation

import (
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/jakinss321/api-getway/internal/modules/notification/application"
	"gitlab.com/jakinss321/api-getway/internal/modules/notification/infrastructure"
	"gitlab.com/jakinss321/api-getway/models"
)

type App struct {
	appService *application.AppService
	tgBot      *infrastructure.TelegramBot
}

func NewAppTgBot(service *application.AppService, tgBot *infrastructure.TelegramBot) *App {
	return &App{
		appService: service,
		tgBot:      tgBot,
	}
}

func (a *App) Run() {
	updateConfig := tgbotapi.NewUpdate(0)
	updateConfig.Timeout = 60
	updates, err := a.tgBot.Bot.GetUpdatesChan(updateConfig)

	if err != nil {
		log.Panic(err)
	}
	go a.InitRabbit()

	for update := range updates {
		if update.Message == nil {
			continue
		}

		if update.Message.IsCommand() {
			if update.Message.Command() == "start" {
				chatID := update.Message.Chat.ID
				user := models.UserTg{ChatID: chatID}
				a.appService.CreateUser(&user)

				msg := tgbotapi.NewMessage(chatID, "Привет! Теперь вы подписаны на уведомления.")
				a.appService.NewMessage(chatID, msg.Text)
			}
		}

	}
}

// else {
// 	// обработка сообщений
// 	var users []models.UserTg
// 	users, err := a.appService.GetUsers()
// 	if err != nil {
// 		log.Println("Error getting users:", err)
// 		continue
// 	}

// 	for _, user := range users {
// 		msg := tgbotapi.NewMessage(user.ChatID, "Новое сообщение: "+update.Message.Text)
// 		a.appService.SendMessage(user.ChatID, msg.Text)
// 	}
// }
