package application

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/jakinss321/api-getway/internal/modules/notification/infrastructure"
	"gitlab.com/jakinss321/api-getway/models"
)

type AppServices interface {
	CreateUser(user *models.UserTg) error
	GetUsers() ([]models.UserTg, error)
	SendMessage(chatID int64, text string) error
}

type AppService struct {
	db    *infrastructure.UserRepository
	tgBot *infrastructure.TelegramBot
}

func NewServiceTg(db *infrastructure.UserRepository, tgBot *infrastructure.TelegramBot) *AppService {
	return &AppService{
		db:    db,
		tgBot: tgBot,
	}
}

func (a *AppService) CreateUser(user *models.UserTg) error {
	return a.db.CreateUser(user)
}
func (a *AppService) GetUsers() ([]models.UserTg, error) {
	return a.db.GetUsers()
}
func (a *AppService) AllUsersMessages(text string) error {
	usrs, err := a.GetUsers()
	for _, usr := range usrs {
		tgMsg := tgbotapi.NewMessage(usr.ChatID, text)
		_, err = a.tgBot.Bot.Send(tgMsg)
	}

	return err
}

func (a *AppService) NewMessage(chatID int64, message string) error {
	tgMsg := tgbotapi.NewMessage(chatID, message)
	_, err := a.tgBot.Bot.Send(tgMsg)
	return err
}
