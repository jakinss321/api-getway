package infrastructure

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"gitlab.com/jakinss321/api-getway/models"
)

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(cfg *models.Cfg) (*UserRepository, error) {
	db, err := gorm.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s sslmode=%s password=%s", cfg.HostDB, cfg.PortDB, cfg.UserDB, cfg.SSLMode, cfg.PasswordDB))
	if err != nil {
		return nil, err
	}
	db.AutoMigrate(&models.UserTg{})

	return &UserRepository{db: db}, nil
}

func (u *UserRepository) CreateUser(usr *models.UserTg) error {
	err := u.db.Create(usr).Error
	if err != nil {
		return err
	}
	return nil
}

func (u *UserRepository) GetUsers() ([]models.UserTg, error) {
	var users []models.UserTg
	err := u.db.Find(&users).Error
	if err != nil {
		return nil, err
	}
	return users, nil
}
