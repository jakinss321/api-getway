package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"

	"gitlab.com/jakinss321/api-getway/configs"
	"gitlab.com/jakinss321/api-getway/internal/handler"
	"gitlab.com/jakinss321/api-getway/internal/middleware/auth"
	"gitlab.com/jakinss321/api-getway/internal/modules/notification/application"
	"gitlab.com/jakinss321/api-getway/internal/modules/notification/infrastructure"
	"gitlab.com/jakinss321/api-getway/internal/modules/notification/presentation"
	"gitlab.com/jakinss321/api-getway/internal/rpc"
	"gitlab.com/jakinss321/api-getway/server"
)

// @title           Documentation of your project API.
// @version         1.0

// @contact.name   api-getway

// @host      localhost:8080

// @securityDefinitions.apiKey ApiKeyAuth
// @in header
// @name Authorization

func main() {

	cfg := configs.InitConfig()
	var conn rpc.RPCClient
	var err error

	if cfg.IfGRPC == "grpc" {
		conn, err = rpc.NewGRPC(cfg)
	} else {
		conn, err = rpc.NewRpc(cfg)
	}
	if err != nil {
		fmt.Println(err)
	}

	userRepository, err := infrastructure.NewUserRepository(cfg)
	if err != nil {
		fmt.Println(err)
	}
	tgBot, err := infrastructure.NewTelegramBot(cfg.Token)
	if err != nil {
		fmt.Println(err)
	}

	serviceTg := application.NewServiceTg(userRepository, tgBot)

	runTgBot := presentation.NewAppTgBot(serviceTg, tgBot)
	go runTgBot.Run()
	auth := auth.AuthsMiddleware(conn)

	router := handler.NewHandler(auth, conn)

	srv := new(server.Server)
	go func() {
		if err := srv.Run("8080", router.InitRoutes()); err != nil {
			logrus.Fatalf("error occured while running http server: %s", err.Error())
		}
	}()

	logrus.Print("Started")

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	logrus.Print("Shutting Down")

	if err := srv.Shutdown(context.Background()); err != nil {
		logrus.Errorf("error occured on server shutting down: %s", err.Error())
	}
}
